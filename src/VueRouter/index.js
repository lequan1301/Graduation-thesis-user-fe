import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Content from "@/components/TheContent/Content";
import SignIn from "@/components/TheContent/SignIn";
import SignUp from "@/components/TheContent/SignUp";
import AccountInfo from "@/components/TheContent/AccountInfo";
import Cart from "@/components/TheContent/Cart";
import Payment from "@/components/TheContent/Payment";
import DetailView from "@/components/Bases/DetailView";
import ProductByCategory from "@/components/Bases/ProductByCategory";
import Search from "@/components/Bases/Search";
const routes = [
  { path: "/", redirect: "/quanlda/home" },
  { path: "/quanlda/home", component: Content },
  { path: "/quanlda/category/:name",name:"productByCategory", component: ProductByCategory},
  { path: "/quanlda/singn", component: SignIn },
  { path: "/quanlda/signup", component: SignUp },
  { path: "/quanlda/infomation", component: AccountInfo },
  { path: "/quanlda/cart", component: Cart },
  { path: "/quanlda/payment", component: Payment },
  { path: "/quanlda/detail/:id", component: DetailView },
  { path: "/quanlda/search", component: Search },
];

const router = new VueRouter({ mode: "history", routes });

export default router;
